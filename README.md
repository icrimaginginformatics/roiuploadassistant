# ROI Upload Assistant

A simple GUI upload client for the XNAT-OHIF viewer plugin. This is a standalone Java application for use on a client workstation.

***!!! DO NOT place the application jar in the XNAT server's plugins directory. This will prevent XNAT from starting correctly !!!***

## Running RUA

RUA requires a Java Virtual Machine compatible with Java 8. Run RUA with the following command:  
```bash
java -jar RoiUploadAssistant-Fat.jar
```

RUA supports ROI collections in DICOM RT Structure Set, DICOM Segmentation and AIM XML formats. The term "ROI collection" is used to indicate a set of one or more ROIs contained within a given file. ROI collections may be contour-based (RTSS, AIM) or mask-based (SEG).

The XNAT-OHIF viewer plugin provides a REST interface for uploading and downloading ROI collections. RUA uses this REST interface to upload ROI collections in exactly the same manner as the viewer plugin itself. Exactly the same result can be achieved by `curl` or a custom REST client. The REST interface is documented in the `ohif-roi-api` section of the XNAT server's Swagger page.

### First Run

RUA uses an encrypted key store. On the first run RUA will create a new key store and will require the user to enter a password for this new key store. Do not forget this password, there is no mechanism to recover it. If the password is lost, the only recourse is to create a new key store.

### Connection Profiles

RUA uses connection profiles to simplify connecting to different XNAT servers. Create or edit profiles using the Profile Manager. The profile is made up of the following fields.

##### Host

The hostname of the XNAT server e.g. `hostname.your.domain`

##### Port

Optional. The port the XNAT server listens on if NOT the standard HTTP (80) or HTTPS (443) ports. Leave empty to use the standard port.

##### Instance

Optional. The part of the XNAT URL that is not the hostname. This is required if XNAT is not the root webapp in the application server.

##### Use SSL

Set this checkbox to use HTTPS (HTTP over SSL) instead of unencrypted HTTP.

##### URL

This field is read-only and shows the full URL that will be used to connect. This is composed of the Host, Port, Instance and SSL fields.

##### Username

The username to connect to the XNAT server with.

##### Password

The password to connect to the XNAT server with. This is stored encrypted in the key store.

##### OK

Click OK to connect to the selected profile. The name of the connected profile is shown in the title bar of the application.

### Project Selection

ROI collections are uploaded to specific projects. All the projects visible to the logged in user are listed in the drop list on the RUA toolbar. Select the project to upload ROI collections to.

### ROI Collection Selection and Upload

Import ROI collections of a particular format via the File menu or toolbar buttons. This will open a directory selection dialog. The selected directory will be scanned for valid ROI collection of the chosen format. Any ROI collections that are found will be displayed in the main UI table. Select the ROI collections you wish to upload in the main table. Once a selection of one or more ROI collections is made, the upload button is enabled. Click the upload button to attempt uploading to the chosen project.

RUA will attempt to find a unique session within the project by using the Study Instance UID read from each ROI collection. Once the session is identified RUA will further check if the session contains the Series Instance UID and all SOP Instance UIDs referenced in the ROI collection. If all conditions are satisfied, RUA will upload the ROI collection to XNAT using the viewer REST API.

If multiple sessions are found that match the Study Instance UID, RUA will ask the user to choose between the compatible sessions.

##### Overwriting ROI Collections

If the ROI collection already exists in the target session, the user will be asked to confirm the overwrite or cancel the upload of the ROI collection.

##### Errors

RUA will report a failed upload if the XNAT server rejects an ROI collection.

**Orphan**: an ROI collection is considered an orphan if no session can be found that matches the Study Instance UID.

**Missing Dependencies**: an ROI collection has missing dependencies if the target session does not contain the Series Instance UID or one or more of the SOP Instance UIDs referenced in the ROI collection. 
