/** *******************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************** */
package roiuploadassistant;

import icr.etherj.FileInstance;
import icr.etherj.PathScanContext;
import icr.etherj.dicom.iod.Iods;
import icr.etherj.dicom.iod.RtStruct;
import java.io.File;
import java.util.List;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.UID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
class RtStructReceiver implements PathScanContext<DicomObject>
{
	private static final Logger logger =
		LoggerFactory.getLogger(RtStructReceiver.class);

	private final List<FileInstance<RtStruct>> instances;

	/**
	 * 
	 * @param instances 
	 */
	public RtStructReceiver(List<FileInstance<RtStruct>> instances)
	{
		this.instances = instances;
	}

	public List<FileInstance<RtStruct>> getInstances()
	{
		return instances;
	}

	@Override
	public void notifyItemFound(File file, DicomObject dcm)
	{
		String sopClassUid = dcm.getString(Tag.SOPClassUID);
		if ((sopClassUid == null) ||
			 !sopClassUid.equals(UID.RTStructureSetStorage))
		{
			return;
		}
		try
		{
			instances.add(new FileInstance(Iods.rtStruct(dcm), file));
		}
		catch (IllegalArgumentException ex)
		{
			logger.warn("Exception caught:", ex);
		}
	}

	@Override
	public void notifyScanFinish()
	{}

	@Override
	public void notifyScanStart()
	{}
	
}
