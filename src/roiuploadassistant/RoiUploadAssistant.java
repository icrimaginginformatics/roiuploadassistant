/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package roiuploadassistant;

import icr.etherj.FileInstance;
import icr.etherj.IoUtils;
import icr.etherj.PathScan;
import icr.etherj.aim.AimFileInstance;
import icr.etherj.aim.AimPathScan;
import icr.etherj.aim.ImageAnnotationCollection;
import icr.etherj.concurrent.CallbackTaskResult;
import icr.etherj.concurrent.Concurrent;
import icr.etherj.concurrent.TaskMonitor;
import icr.etherj.crypto.CryptoException;
import icr.etherj.dicom.DicomToolkit;
import icr.etherj.dicom.iod.RtStruct;
import icr.etherj.dicom.iod.Segmentation;
import icr.etherj.ui.TaskMonitorDialog;
import icr.etherj.xnat.DefaultXnatUploader;
import icr.etherj.xnat.IacUploadHelper;
import icr.etherj.xnat.IacUploadMetadataV2;
import icr.etherj.xnat.RtStructUploadHelper;
import icr.etherj.xnat.RtStructUploadMetadata;
import icr.etherj.xnat.SegmentationUploadHelper;
import icr.etherj.xnat.SegmentationUploadMetadata;
import icr.etherj.xnat.UploadMetadata;
import icr.etherj.xnat.impl.XapiXnatUploader;
import icr.etherj.xnat.XnatException;
import icr.etherj.xnat.XnatUploader;
import icr.etherj.xnat.XnatUploader.Result;
import icr.etherj.xnat.app.ApplicationState;
import icr.etherj.xnat.app.FunctionConsumerCallbackTask;
import icr.etherj.xnat.app.XnatSwingApplication;
import icr.etherj.xnat.ui.UploadMonitorDialog;
import icr.etherj.xnat.ui.UploadReportDialog;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.TableColumnModel;
import org.apache.log4j.Logger;
import org.dcm4che2.data.DicomObject;
import roiuploadassistant.Options.Geometry;

/**
 *
 * @author jamesd
 */
public class RoiUploadAssistant extends XnatSwingApplication
{
	private static final Logger logger = Logger.getLogger(
		RoiUploadAssistant.class);

	private Action aimImportAction;
	private JPanel contentPane;
	private JDialog uploadDialog;
	private Options options;
	private final String optionsFile;
	private String projectId;
	private RoiTableModel roiTableModel;
	private Action rtImportAction;
	private Action segImportAction;
	private JTable table;
	private JScrollPane tableScroll;
	private Action uploadAction;
	private Class uploadClass;

	private final boolean useXapi = true;

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args)
	{
		SwingUtilities.invokeLater(() ->
		{
			try
			{
				RoiUploadAssistant app = new RoiUploadAssistant();
				app.startup();
			}
			catch (CryptoException | IOException ex)
			{
				logger.error("RoiUploadAssistant error", ex);
				JOptionPane.showMessageDialog(null, ex.getMessage(),
					"RoiUploadAssistant Error", JOptionPane.ERROR);
			}
		});
	}

	@Override
	public void onReadConfig() throws IOException
	{
		String path = getAppPath()+optionsFile;
		Properties p = new Properties();
		try
		{
			IoUtils.ensureDirExists(new File(path).getParentFile());
			p.loadFromXML(new FileInputStream(path));
			logger.info("Options read from "+path);
		}
		catch (FileNotFoundException ex)
		{
			onWriteConfig();
		}
		catch (IOException ex)
		{
			logger.warn("Cannot read options from "+path, ex);
		}
		options = new Options(p);
	}

	@Override
	public void onProjectSelectionChanged(PropertyChangeEvent event)
	{
		System.out.println("OldValue: "+event.getOldValue()+", NewValue: "+
			event.getNewValue());
		setTargetProject();
	}

	@Override
	public void onShowUi()
	{
		
	}

	@Override
	public void onStartupComplete()
	{
		
	}

	@Override
	public void onStateChanged(PropertyChangeEvent evt)
	{
		String newState = (String) evt.getNewValue();
		logger.info("State changed to: <"+newState+">");
		switch (newState)
		{
			case ApplicationState.NoKeystore:
				break;

			case ApplicationState.Disconnected:
				aimImportAction.setEnabled(false);
				rtImportAction.setEnabled(false);
				segImportAction.setEnabled(false);
				uploadAction.setEnabled(false);
				roiTableModel.clear();
				break;

			case ApplicationState.Authenticated:
				break;

			case ApplicationState.AuthWithProjects:
				String selectedProject = options.getProjectSelection(
					getProfileManager().getCurrentProfileName());
				if (!selectedProject.isEmpty())
				{
					getProjectSelector().setSelection(selectedProject);
				}
				setTargetProject();
				aimImportAction.setEnabled(true);
				rtImportAction.setEnabled(true);
				segImportAction.setEnabled(true);
				break;

			default:
		}
	}

	@Override
	public void onUiCreation()
	{
		Geometry geometry = options.getGeometry();

		JFrame frame = getFrame();
		frame.addComponentListener(new ComponentAdapter());

		contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout());
		contentPane.setBorder(
			BorderFactory.createMatteBorder(1, 0, 0, 0, Color.gray));
		frame.add(contentPane, BorderLayout.CENTER);

		frame.setLocation(geometry.getFrameX(), geometry.getFrameY());
		int height = geometry.getContentHeight();
		int width = geometry.getContentWidth();
		if ((height < 100) || (width < 150))
		{
			height = 800;
			width = 1000;
		}
		contentPane.setPreferredSize(new Dimension(width, height));
		if (geometry.isMaximised())
		{
			frame.setExtendedState(frame.getExtendedState() | JFrame.MAXIMIZED_BOTH);
		}

		createActions();
		createMenus(frame);
		createToolbar(frame);

		initTable();
	}

	@Override
	public void onWriteConfig()
	{
		readGeometry();
		File appDir = new File(getAppPath());
		String path = appDir+File.separator+optionsFile;
		Properties p = options.toProperties();
		try
		{
			if (!appDir.exists())
			{
				appDir.mkdirs();
			}
			p.storeToXML(new FileOutputStream(path), getClass().getSimpleName());
			logger.info("Options saved to "+path);
		}
		catch (IOException ex)
		{
			logger.warn("Cannot save options to "+path, ex);
		}
	}

	@Override
	protected void handleNetIoException(Exception ex)
	{
		super.handleNetIoException(ex);
	}

	/* **************************************************************************
	 *	Private methods
	 ***************************************************************************/

	private RoiUploadAssistant() throws CryptoException, IllegalArgumentException
	{
		super("RoiUploadAssistant");
		options = new Options();
		optionsFile = getProductName()+".properties";
	}

	private void createActions()
	{
		aimImportAction = new AimImportAction();
		aimImportAction.putValue(Action.LARGE_ICON_KEY,
			new ImageIcon(getClass().getResource(
				"/icr/etherj/ui/resources/Crosshair24.png")));
		aimImportAction.setEnabled(false);
		rtImportAction = new RtImportAction();
		rtImportAction.putValue(Action.LARGE_ICON_KEY,
			new ImageIcon(getClass().getResource(
				"/icr/etherj/ui/resources/BlueRoi24.png")));
		rtImportAction.setEnabled(false);
		segImportAction = new SegImportAction();
		segImportAction.putValue(Action.LARGE_ICON_KEY,
			new ImageIcon(getClass().getResource(
				"/icr/etherj/ui/resources/ThreeSpheres24.png")));
		segImportAction.setEnabled(false);
		uploadAction = new UploadAction();
		uploadAction.putValue(Action.LARGE_ICON_KEY,
			new ImageIcon(getClass().getResource(
				"/icr/etherj/ui/resources/GreenUpArrow24.png")));
		uploadAction.setEnabled(false);
	}

	private void createMenus(JFrame frame)
	{
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		JMenu fileMenu = new JMenu("File");
		insertProfileItems(fileMenu);
		fileMenu.add(new JSeparator());

		JMenuItem aimImportItem = new JMenuItem(aimImportAction);
		JMenuItem rtImportItem = new JMenuItem(rtImportAction);
		JMenuItem segImportItem = new JMenuItem(segImportAction);
		JMenuItem uploadItem = new JMenuItem(uploadAction);
		fileMenu.add(aimImportItem);
		fileMenu.add(rtImportItem);
		fileMenu.add(segImportItem);
		fileMenu.add(uploadItem);

		fileMenu.add(new JSeparator());
		JMenuItem quit = new JMenuItem("Quit");
		quit.addActionListener((ActionEvent) -> frame.dispatchEvent(
			new WindowEvent(frame, WindowEvent.WINDOW_CLOSING)));
		fileMenu.add(quit);

		menuBar.add(fileMenu);
	}

	private void createToolbar(JFrame frame)
	{
		JToolBar tb = new JToolBar();
		tb.setRollover(true);
		tb.setFloatable(false);

		insertProfileItems(tb);
		tb.addSeparator();

		tb.add(aimImportAction);
		tb.add(rtImportAction);
		tb.add(segImportAction);
		tb.addSeparator();
		tb.add(uploadAction);
		tb.addSeparator();

		frame.add(tb, BorderLayout.NORTH);
	}

	private void initTable()
	{
		roiTableModel = new RoiTableModel();
		table = new JTable(roiTableModel);
		tableScroll = new JScrollPane(table);
		table.setFillsViewportHeight(true);
		table.getSelectionModel().addListSelectionListener(this::onTableSelection);
		table.setAutoCreateRowSorter(true);
		contentPane.add(tableScroll, BorderLayout.CENTER);

		Geometry geometry = options.getGeometry();
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		TableColumnModel colModel = table.getColumnModel();
		int width = geometry.getColumnWidth(0);
		colModel.getColumn(0).setPreferredWidth((width > 0) ? width : 150);
		width = geometry.getColumnWidth(1);
		colModel.getColumn(1).setPreferredWidth((width > 0) ? width : 150);
		width = geometry.getColumnWidth(2);
		colModel.getColumn(2).setPreferredWidth((width > 0) ? width : 420);
		width = geometry.getColumnWidth(3);
		colModel.getColumn(3).setPreferredWidth((width > 0) ? width : 100);
		width = geometry.getColumnWidth(4);
		colModel.getColumn(4).setPreferredWidth((width > 0) ? width : 75);
		width = geometry.getColumnWidth(5);
		colModel.getColumn(5).setPreferredWidth((width > 0) ? width : 100);
	}

	private void onImportAim()
	{
		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle("AIM Search");
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		String input = options.getInputPath();
		if ((input != null) && (!input.isEmpty()))
		{
			fc.setCurrentDirectory(new File(input));
		}
		JFrame frame = getFrame();
		int choice = fc.showOpenDialog(frame);
		if (choice != JFileChooser.APPROVE_OPTION)
		{
			return;
		}
		File aimDir = fc.getSelectedFile();
		options.setInputPath(aimDir.getPath());
		onWriteConfig();
		roiTableModel.clear();
		uploadClass = ImageAnnotationCollection.class;

		frame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		FunctionConsumerCallbackTask<File,List<AimFileInstance>> task =
			new FunctionConsumerCallbackTask<>(
				this::scanAimDir,
				this::scanAimDirDone,
				aimDir);
		getNetworkExecutorService().submit(task);
	}

	private void onImportRtStruct()
	{
		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle("RTSTRUCT Search");
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		String input = options.getInputPath();
		if ((input != null) && (!input.isEmpty()))
		{
			fc.setCurrentDirectory(new File(input));
		}
		JFrame frame = getFrame();
		int choice = fc.showOpenDialog(frame);
		if (choice != JFileChooser.APPROVE_OPTION)
		{
			return;
		}
		File rtDir = fc.getSelectedFile();
		options.setInputPath(rtDir.getPath());
		onWriteConfig();
		roiTableModel.clear();
		uploadClass = RtStruct.class;

		frame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		FunctionConsumerCallbackTask<File,List<FileInstance<RtStruct>>> task =
			new FunctionConsumerCallbackTask<>(
				this::scanRtDir,
				this::scanRtDirDone,
				rtDir);
		getNetworkExecutorService().submit(task);
	}

	private void onImportSeg()
	{
		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle("DICOM SEG Search");
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		String input = options.getInputPath();
		if ((input != null) && (!input.isEmpty()))
		{
			fc.setCurrentDirectory(new File(input));
		}
		JFrame frame = getFrame();
		int choice = fc.showOpenDialog(frame);
		if (choice != JFileChooser.APPROVE_OPTION)
		{
			return;
		}
		File segDir = fc.getSelectedFile();
		options.setInputPath(segDir.getPath());
		onWriteConfig();
		roiTableModel.clear();
		uploadClass = Segmentation.class;

		frame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		FunctionConsumerCallbackTask<File,List<FileInstance<Segmentation>>> task =
			new FunctionConsumerCallbackTask<>(
				this::scanSegDir,
				this::scanSegDirDone,
				segDir);
		getNetworkExecutorService().submit(task);
	}

	private void onTableSelection(ListSelectionEvent event)
	{
		if (event.getValueIsAdjusting())
		{
			return;
		}
		uploadAction.setEnabled(table.getSelectedRowCount() > 0);
	}

	private void onUpload()
	{
		switch (uploadClass.getCanonicalName())
		{
			case "icr.etherj.aim.ImageAnnotationCollection":
				onUploadIacs();
				break;
			case "icr.etherj.dicom.iod.RtStruct":
				onUploadRtStructs();
				break;
			case "icr.etherj.dicom.iod.Segmentation":
				onUploadSegs();
				break;
			default:
				logger.warn("This should never happen. Unknown upload class: "+
					uploadClass.getCanonicalName());
		}
	}

	private void onUploadIacs()
	{
		logger.info("IAC upload to "+projectId);
		List<AimFileInstance> instances = new ArrayList<>();
		int[] selection = table.getSelectedRows();
		for (int idx : selection)
		{
			int modelIdx = table.convertRowIndexToModel(idx);
			instances.add(
				(AimFileInstance) roiTableModel.getItem(modelIdx).getOriginalItem());
		}
		FunctionConsumerCallbackTask<List<AimFileInstance>,
			List<Result<ImageAnnotationCollection>>> task =
			new FunctionConsumerCallbackTask<>(
				this::uploadIacs,
				this::uploadIacsDone,
				instances);
			getNetworkExecutorService().submit(task);
	}

	private void onUploadRtStructs()
	{
		logger.info("RTSTRUCT upload to "+projectId);
		List<FileInstance<RtStruct>> instances = new ArrayList<>();
		int[] selection = table.getSelectedRows();
		for (int idx : selection)
		{
			int modelIdx = table.convertRowIndexToModel(idx);
			instances.add((FileInstance<RtStruct>)
				roiTableModel.getItem(modelIdx).getOriginalItem());
		}
		FunctionConsumerCallbackTask<List<FileInstance<RtStruct>>,
			List<Result<RtStruct>>> task =
			new FunctionConsumerCallbackTask<>(
				this::uploadRtStructs,
				this::uploadRtStructsDone,
				instances);
			getNetworkExecutorService().submit(task);
	}

	private void onUploadSegs()
	{
		logger.info("DICOM SEG upload to "+projectId);
		List<FileInstance<Segmentation>> instances = new ArrayList<>();
		int[] selection = table.getSelectedRows();
		for (int idx : selection)
		{
			int modelIdx = table.convertRowIndexToModel(idx);
			instances.add((FileInstance<Segmentation>)
				roiTableModel.getItem(modelIdx).getOriginalItem());
		}
		FunctionConsumerCallbackTask<List<FileInstance<Segmentation>>,
			List<Result<Segmentation>>> task =
			new FunctionConsumerCallbackTask<>(
				this::uploadSegs,
				this::uploadSegsDone,
				instances);
			getNetworkExecutorService().submit(task);
	}

	private void readGeometry()
	{
		Geometry geometry = options.getGeometry();
		JFrame frame = getFrame();
		if (frame == null)
		{
			return;
		}
		boolean isMax = (frame.getExtendedState() & JFrame.MAXIMIZED_BOTH) != 0;
		if (!isMax)
		{
			Point p = frame.getLocation();
			geometry.setFrameX(p.x);
			geometry.setFrameY(p.y);
			Dimension d = contentPane.getSize();
			geometry.setContentHeight(d.height);
			geometry.setContentWidth(d.width);
		}
		geometry.setMaximised(isMax);
		TableColumnModel model = table.getColumnModel();
		for (int i=0; i<model.getColumnCount(); i++)
		{
			geometry.setColumnWidth(i, model.getColumn(i).getWidth());
		}
	}

	private List<AimFileInstance> scanAimDir(File aimDir)
	{
		logger.info("IAC search: "+aimDir);
		List<AimFileInstance> instances = new ArrayList<>();
		AimReceiver aimRx = new AimReceiver(instances);
		AimPathScan pathScan = new AimPathScan();
		pathScan.addContext(aimRx);
		TaskMonitor taskMonitor = Concurrent.getTaskMonitor();
		taskMonitor.setTitle("Scanning");
		TaskMonitorDialog monitorDialog =
			new TaskMonitorDialog(getFrame(), taskMonitor);
		monitorDialog.setVisible(true);
		try
		{
			pathScan.scan(aimDir.getPath(), true, taskMonitor);
		}
		catch (IOException ex)
		{
			logger.warn(ex.getMessage(), ex);
		}
		monitorDialog.dispose();
		logger.info(instances.size()+" IACs found");
		return instances;
	}

	private void scanAimDirDone(CallbackTaskResult<List<AimFileInstance>> result)
	{
		JFrame frame = getFrame();
		frame.setCursor(Cursor.getDefaultCursor());
		if (!result.success())
		{
			handleNetIoException(result.getException());
			return;
		}
		List<AimFileInstance> instances = result.get();
		List<RoiTableItem> items = new ArrayList<>();
		instances.forEach((instance) -> items.add(new IacRoiTableItem(instance)));
		roiTableModel.addAll(items);
	}

	private List<FileInstance<RtStruct>> scanRtDir(File rtDir)
	{
		logger.info("RTSTRUCT search: "+rtDir);
		List<FileInstance<RtStruct>> instances = new ArrayList<>();
		RtStructReceiver dcmRx = new RtStructReceiver(instances);
		PathScan<DicomObject> pathScan = DicomToolkit.getToolkit().createPathScan();
		pathScan.addContext(dcmRx);
		TaskMonitor taskMonitor = Concurrent.getTaskMonitor();
		taskMonitor.setTitle("Scanning");
		TaskMonitorDialog monitorDialog =
			new TaskMonitorDialog(getFrame(), taskMonitor);
		monitorDialog.setVisible(true);
		try
		{
			pathScan.scan(rtDir.getPath(), true, taskMonitor);
		}
		catch (IOException ex)
		{
			logger.warn(ex.getMessage(), ex);
		}
		monitorDialog.dispose();
		logger.info(instances.size()+" RTSTRUCTs found");
		return instances;
	}

	private void scanRtDirDone(
		CallbackTaskResult<List<FileInstance<RtStruct>>> result)
	{
		JFrame frame = getFrame();
		frame.setCursor(Cursor.getDefaultCursor());
		if (!result.success())
		{
			handleNetIoException(result.getException());
			return;
		}
		List<FileInstance<RtStruct>> instances = result.get();
		List<RoiTableItem> items = new ArrayList<>();
		instances.forEach((instance) ->
			items.add(new RtStructRoiTableItem(instance)));
		roiTableModel.addAll(items);
	}

	private List<FileInstance<Segmentation>> scanSegDir(File segDir)
	{
		logger.info("DICOM SEG search: "+segDir);
		List<FileInstance<Segmentation>> instances = new ArrayList<>();
		SegmentationReceiver segRx = new SegmentationReceiver(instances);
		PathScan<DicomObject> pathScan = DicomToolkit.getToolkit().createPathScan();
		pathScan.addContext(segRx);
		TaskMonitor taskMonitor = Concurrent.getTaskMonitor();
		taskMonitor.setTitle("Scanning");
		TaskMonitorDialog monitorDialog =
			new TaskMonitorDialog(getFrame(), taskMonitor);
		monitorDialog.setVisible(true);
		try
		{
			pathScan.scan(segDir.getPath(), true, taskMonitor);
		}
		catch (IOException ex)
		{
			logger.warn(ex.getMessage(), ex);
		}
		monitorDialog.dispose();
		logger.info(instances.size()+" DICOM SEGs found");
		return instances;
	}

	private void scanSegDirDone(
		CallbackTaskResult<List<FileInstance<Segmentation>>> result)
	{
		JFrame frame = getFrame();
		frame.setCursor(Cursor.getDefaultCursor());
		if (!result.success())
		{
			handleNetIoException(result.getException());
			return;
		}
		List<FileInstance<Segmentation>> instances = result.get();
		List<RoiTableItem> items = new ArrayList<>();
		instances.forEach((instance) ->
			items.add(new SegmentationRoiTableItem(instance)));
		roiTableModel.addAll(items);
	}

	private void setTargetProject()
	{
		String[] selection = getProjectSelector().getSelection();
		projectId = (selection.length > 0) ? selection[0] : null;
		if ((projectId != null) && !projectId.isEmpty())
		{
			options.setProjectSelection(
				getProfileManager().getCurrentProfileName(), projectId);
		}
		logger.info("Project ID: "+projectId);
	}

	private List<Result<ImageAnnotationCollection>> uploadIacs(
		List<AimFileInstance> instances) throws XnatException
	{
		logger.info("AIM upload");
		UploadMonitorDialog<ImageAnnotationCollection> uploadMonitor =
			new UploadMonitorDialog<>(getFrame(),
				"AIM Upload to Project: "+projectId);
		uploadDialog = uploadMonitor;
		XnatUploader<ImageAnnotationCollection> uploader;
		if (useXapi)
		{
			uploader = new XapiXnatUploader<>(getConnection(),
				uploadMonitor.getTaskMonitor());
		}
		else
		{
			uploader = new DefaultXnatUploader<>(getConnection(),
				uploadMonitor.getTaskMonitor());
		}
		List<UploadMetadata<ImageAnnotationCollection>> iacMetadataList =
			new ArrayList<>();
		XnatUploader.Helper<ImageAnnotationCollection> helper;
		instances.forEach((item) -> iacMetadataList.add(
			new IacUploadMetadataV2(item.getImageAnnotationCollection(),
				item.getFile())));
		helper = new IacUploadHelper();
		SwingUtilities.invokeLater(() ->
		{
			uploadMonitor.setVisible(true);
		});
		List<Result<ImageAnnotationCollection>> resultList =
			uploader.upload(projectId, iacMetadataList, helper, uploadMonitor);
		return resultList;
	}

	private void uploadIacsDone(
		CallbackTaskResult<List<Result<ImageAnnotationCollection>>> result)
	{
		if (uploadDialog != null)
		{
			uploadDialog.dispose();
			uploadDialog = null;
		}
		if (!result.success())
		{
			handleNetIoException(result.getException());
			return;
		}
		List<Result<ImageAnnotationCollection>> iacResultList = result.get();
		boolean fail = false;
		for (Result<ImageAnnotationCollection> iacResult : iacResultList)
		{
			if (iacResult.getStatus() != Result.Success)
			{
				fail = true;
			}
		}
		if (fail)
		{
			UploadReportDialog<ImageAnnotationCollection> dialog = 
				new UploadReportDialog<>(getFrame(), "AIM Upload Report",
					iacResultList);
			SwingUtilities.invokeLater(() -> dialog.setVisible(true));
		}
	}

	private List<Result<RtStruct>> uploadRtStructs(
		List<FileInstance<RtStruct>> rtStructs) throws XnatException
	{
		logger.info("RTSTRUCT upload");
		UploadMonitorDialog<RtStruct> uploadMonitor =
			new UploadMonitorDialog<>(getFrame(),
				"RTSTRUCT Upload to Project: "+projectId);
		uploadDialog = uploadMonitor;
		XnatUploader<RtStruct> uploader;
		if (useXapi)
		{
			uploader = new XapiXnatUploader<>(getConnection(),
				uploadMonitor.getTaskMonitor());
		}
		else
		{
			uploader = new DefaultXnatUploader<>(getConnection(),
				uploadMonitor.getTaskMonitor());
		}
		List<UploadMetadata<RtStruct>> metadataList = new ArrayList<>();
		rtStructs.forEach((item) -> metadataList.add(
			new RtStructUploadMetadata(item.getObject(), item.getFile())));
		XnatUploader.Helper<RtStruct> helper = new RtStructUploadHelper();
		SwingUtilities.invokeLater(() ->
		{
			uploadMonitor.setVisible(true);
		});
		List<Result<RtStruct>> resultList =
			uploader.upload(projectId, metadataList, helper, uploadMonitor);
		return resultList;
	}

	private void uploadRtStructsDone(
		CallbackTaskResult<List<Result<RtStruct>>> result)
	{
		if (uploadDialog != null)
		{
			uploadDialog.dispose();
			uploadDialog = null;
		}
		if (!result.success())
		{
			handleNetIoException(result.getException());
			return;
		}
		List<Result<RtStruct>> rtResultList = result.get();
		boolean fail = false;
		for (Result<RtStruct> rtResult : rtResultList)
		{
			if (rtResult.getStatus() != Result.Success)
			{
				fail = true;
			}
		}
		if (fail)
		{
			UploadReportDialog<RtStruct> dialog = new UploadReportDialog<>(
				getFrame(), "RTSTRUCT Upload Report", rtResultList);
			SwingUtilities.invokeLater(() -> dialog.setVisible(true));
		}
	}

	private List<Result<Segmentation>> uploadSegs(
		List<FileInstance<Segmentation>> segs) throws XnatException
	{
		logger.info("DICOM SEG upload");
		UploadMonitorDialog<Segmentation> uploadMonitor =
			new UploadMonitorDialog<>(getFrame(),
				"DICOM SEG Upload to Project: "+projectId);
		uploadDialog = uploadMonitor;
		XnatUploader<Segmentation> uploader;
		if (useXapi)
		{
			uploader = new XapiXnatUploader<>(getConnection(),
				uploadMonitor.getTaskMonitor());
		}
		else
		{
			uploader = new DefaultXnatUploader<>(getConnection(),
				uploadMonitor.getTaskMonitor());
		}
		List<UploadMetadata<Segmentation>> metadataList = new ArrayList<>();
		segs.forEach((item) -> metadataList.add(
			new SegmentationUploadMetadata(item.getObject(), item.getFile())));
		XnatUploader.Helper<Segmentation> helper = new SegmentationUploadHelper();
		SwingUtilities.invokeLater(() ->
		{
			uploadMonitor.setVisible(true);
		});
		List<Result<Segmentation>> resultList =
			uploader.upload(projectId, metadataList, helper, uploadMonitor);
		return resultList;
	}

	private void uploadSegsDone(
		CallbackTaskResult<List<Result<Segmentation>>> result)
	{
		if (uploadDialog != null)
		{
			uploadDialog.dispose();
			uploadDialog = null;
		}
		if (!result.success())
		{
			handleNetIoException(result.getException());
			return;
		}
		List<Result<Segmentation>> segResultList = result.get();
		boolean fail = false;
		for (Result<Segmentation> segResult : segResultList)
		{
			if (segResult.getStatus() != Result.Success)
			{
				fail = true;
			}
		}
		if (fail)
		{
			UploadReportDialog<Segmentation> dialog = new UploadReportDialog<>(
				getFrame(), "DICOM SEG Upload Report", segResultList);
			SwingUtilities.invokeLater(() -> dialog.setVisible(true));
		}
	}

	private final class AimImportAction extends AbstractAction
	{
		AimImportAction()
		{
			super("AIM Import...");
			putValue(Action.SHORT_DESCRIPTION,
				"Import AIM Image Annotation Collections");
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			onImportAim();
		}
	}

	private final class ComponentAdapter implements ComponentListener
	{
		@Override
		public void componentHidden(ComponentEvent e)
		{}

		@Override
		public void componentMoved(ComponentEvent e)
		{
			readGeometry();
		}

		@Override
		public void componentResized(ComponentEvent e)
		{
			readGeometry();
		}

		@Override
		public void componentShown(ComponentEvent e)
		{}
		
	}

	private final class RtImportAction extends AbstractAction
	{
		RtImportAction()
		{
			super("RTSTRUCT Import...");
			putValue(Action.SHORT_DESCRIPTION, "Import RT Structure Sets");
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			onImportRtStruct();
		}
	}

	private final class SegImportAction extends AbstractAction
	{
		SegImportAction()
		{
			super("DICOM SEG Import...");
			putValue(Action.SHORT_DESCRIPTION, "Import DICOM SEGs");
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			onImportSeg();
		}
	}

	private final class UploadAction extends AbstractAction
	{
		UploadAction()
		{
			super("Upload...");
			putValue(Action.SHORT_DESCRIPTION, "Upload selected ROIs to XNAT");
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			onUpload();
		}
	}

}
