/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package roiuploadassistant;

import com.google.common.collect.ImmutableSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *
 * @author jamesd
 */
public class Options
{
	private final static String GEOMETRY = "roiuploadassistant.geometry";
	private final static String INPUTPATH = "roiuploadassistant.inputPath";
	private final static String OUTPUTPATH = "roiuploadassistant.outputPath";
	private final static String PROFILENAME = "roiuploadassistant.profileName";
	private final static String PROJECT_SELECTIONS = "roiuploadassistant.projectSelections";
	private final static String COLUMN = "column";
	private final static String CONTENT_HEIGHT = "contentheight";
	private final static String CONTENT_WIDTH = "contentwidth";
	private final static String COUNT = "count";
	private final static String FRAME_X = "framex";
	private final static String FRAME_Y = "framey";
	private final static String MAX = "max";
	private final static String NAME = "name";
	private final static String PROFILE = "profile";
	private final static String SELECTION = "selection";

	private final Geometry geometry = new Geometry();
	private String inputPath;
	private String outputPath;
	private String profileName;
	private final Map<String,String> projectSelections = new HashMap<>();

	/**
	 *
	 */
	public Options()
	{
		inputPath = "";
		outputPath = "";
		profileName = "";
	}

	/**
	 *
	 * @param p
	 */
	public Options(Properties p)
	{
		inputPath = p.getProperty(INPUTPATH, "");
		outputPath = p.getProperty(OUTPUTPATH, "");
		profileName = p.getProperty(PROFILENAME, "");
		String stub = PROJECT_SELECTIONS+".";
		int nProjects = getIntProperty(p, stub+COUNT, 0);
		for (int i=0; i<nProjects; i++)
		{
			String projectStub = stub+String.format("%s%d.", PROFILE, i);
			String name = p.getProperty(projectStub+NAME, "");
			String project = p.getProperty(projectStub+SELECTION, "");
			if (!name.isEmpty() && !project.isEmpty())
			{
				projectSelections.put(name, project);
			}
		}
		stub = GEOMETRY+".";
		geometry.setFrameX(getIntProperty(p, stub+FRAME_X, 0));
		geometry.setFrameY(getIntProperty(p, stub+FRAME_Y, 0));
		geometry.setContentHeight(getIntProperty(p, stub+CONTENT_HEIGHT, 0));
		geometry.setContentWidth(getIntProperty(p, stub+CONTENT_WIDTH, 0));
		geometry.setMaximised(Boolean.parseBoolean(p.getProperty(stub+MAX)));
		stub += COLUMN+".";
		int idx = 0;
		int width = getIntProperty(p, stub+String.format("%d", idx), 0);
		while (width > 0)
		{
			geometry.setColumnWidth(idx, width);
			idx++;
			width = getIntProperty(p, stub+String.format("%d", idx), 0);
		}
	}

	/**
	 *
	 * @return
	 */
	public Geometry getGeometry()
	{
		return geometry;
	}

	/**
	 * @return the inputPath
	 */
	public String getInputPath()
	{
		return inputPath;
	}

	/**
	 * @return the outputPath
	 */
	public String getOutputPath()
	{
		return outputPath;
	}

	/**
	 * @return the profileName
	 */
	public String getProfileName()
	{
		return profileName;
	}

	/**
	 *
	 * @param profileName
	 * @return
	 */
	public String getProjectSelection(String profileName)
	{
		return projectSelections.getOrDefault(profileName, "");
	}

	public Set<String> getProjects()
	{
		return Collections.unmodifiableSet(projectSelections.keySet());
	}

	/**
	 * @param inputPath the inputPath to set
	 */
	public void setInputPath(String inputPath)
	{
		this.inputPath = inputPath;
	}

	/**
	 * @param outputPath the outputPath to set
	 */
	public void setOutputPath(String outputPath)
	{
		this.outputPath = outputPath;
	}

	/**
	 * @param profileName the profileName to set
	 */
	public void setProfileName(String profileName)
	{
		this.profileName = profileName;
	}

	/**
	 *
	 * @param profileName
	 * @param projectName
	 * @return
	 */
	public String setProjectSelection(String profileName, String projectName)
	{
		return projectSelections.put(profileName, projectName);
	}

	/**
	 *
	 * @return
	 */
	public Properties toProperties()
	{
		Properties p = new Properties();
		p.setProperty(INPUTPATH, inputPath);
		p.setProperty(OUTPUTPATH, outputPath);
		p.setProperty(PROFILENAME, profileName);
		int nProjects = projectSelections.size();
		String stub = PROJECT_SELECTIONS+".";
		p.setProperty(stub+COUNT, String.valueOf(nProjects));
		int i=0;
		for (String name : projectSelections.keySet())
		{
			String projectStub = stub+String.format("%s%d.", PROFILE, i);
			p.setProperty(projectStub+NAME, name);
			p.setProperty(projectStub+SELECTION, projectSelections.get(name));
			i++;
		}
		stub = GEOMETRY+".";
		p.setProperty(stub+FRAME_X, String.valueOf(geometry.getFrameX()));
		p.setProperty(stub+FRAME_Y, String.valueOf(geometry.getFrameY()));
		p.setProperty(stub+CONTENT_HEIGHT,
			String.valueOf(geometry.getContentHeight()));
		p.setProperty(stub+CONTENT_WIDTH,
			String.valueOf(geometry.getContentWidth()));
		p.setProperty(stub+MAX, String.valueOf(geometry.isMaximised()));
		stub += COLUMN+".";
		for (Integer idx : geometry.getColumnIndices())
		{
			p.setProperty(stub+String.format("%d", idx),
				String.valueOf(geometry.getColumnWidth(idx)));
		}

		return p;
	}

	private int getIntProperty(Properties p, String name, int minValue)
	{
		int value = 0;
		try
		{
			value = Integer.parseInt(p.getProperty(name));
			if (value < minValue)
			{
				value = 0;
			}
		}
		catch (NumberFormatException ex)
		{}
		return value;
	}

	/**
	 *
	 */
	public static class Geometry
	{
		private boolean maximised = false;
		private int frameX = 0;
		private int frameY = 0;
		private int contentWidth = 0;
		private int contentHeight = 0;
		private final SortedMap<Integer,Integer> columnWidths = new TreeMap<>();

		/**
		 * @return the columnWidths
		 */
		public Set<Integer> getColumnIndices()
		{
			return ImmutableSet.copyOf(columnWidths.keySet());
		}

		/**
		 * @param idx
		 * @return the width for column idx
		 */
		public int getColumnWidth(int idx)
		{
			Integer width = columnWidths.get(idx);
			return (width != null) ? width : -1;
		}

		/**
		 * @return the contentHeight
		 */
		public int getContentHeight()
		{
			return contentHeight;
		}

		/**
		 * @return the contentWidth
		 */
		public int getContentWidth()
		{
			return contentWidth;
		}

		/**
		 * @return the frameX
		 */
		public int getFrameX()
		{
			return frameX;
		}

		/**
		 * @return the frameY
		 */
		public int getFrameY()
		{
			return frameY;
		}

		/**
		 * @return the maximised
		 */
		public boolean isMaximised()
		{
			return maximised;
		}

		/**
		 *
		 * @param idx
		 * @param width
		 */
		public void setColumnWidth(int idx, int width)
		{
			columnWidths.put(idx, width);
		}

		/**
		 * @param contentHeight the contentHeight to set
		 */
		public void setContentHeight(int contentHeight)
		{
			if (contentHeight >= 100)
			{
				this.contentHeight = contentHeight;
			}
		}

		/**
		 * @param contentWidth the contentWidth to set
		 */
		public void setContentWidth(int contentWidth)
		{
			if (contentWidth >= 150)
			{
				this.contentWidth = contentWidth;
			}
		}

		/**
		 * @param frameX the frameX to set
		 */
		public void setFrameX(int frameX)
		{
			if (frameX >= 0)
			{
				this.frameX = frameX;
			}
		}

		/**
		 * @param frameY the frameY to set
		 */
		public void setFrameY(int frameY)
		{
			if (frameY >= 0)
			{
				this.frameY = frameY;
			}
		}

		/**
		 * @param maximised the maximised to set
		 */
		public void setMaximised(boolean maximised)
		{
			this.maximised = maximised;
		}
	}

}
