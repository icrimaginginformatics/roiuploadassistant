/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package roiuploadassistant;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author jamesd
 */
class RoiTableModel extends AbstractTableModel
{
	private String[] colNames = new String[] {
		"Person Name",
		"Person ID",
		"ROI Name",
		"Date",
		"Time",
		"Type"
	};
	private List<RoiTableItem> items = new ArrayList<>();

	public boolean addAll(List<RoiTableItem> list)
	{
		if (list.isEmpty())
		{
			return false;
		}
		int first = items.size();
		boolean b = items.addAll(list);
		fireTableRowsInserted(first, items.size()-1);
		return b;
	}

	public void clear()
	{
		items.clear();
		fireTableDataChanged();
	}

	@Override
	public Class<?> getColumnClass(int idx)
	{
		return ((idx < 0) || (idx >= colNames.length)) ? Object.class : String.class;
	}

	@Override
	public int getColumnCount()
	{
		return colNames.length;
	}

	@Override
	public String getColumnName(int idx)
	{
		return ((idx < 0) || (idx >= colNames.length)) ? "" : colNames[idx];
	}

	public RoiTableItem getItem(int idx)
	{
		return items.get(idx);
	}

	@Override
	public int getRowCount()
	{
		return items.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex)
	{
		if ((rowIndex < 0) || (rowIndex >= items.size()))
		{
			return null;
		}
		RoiTableItem item = items.get(rowIndex);
		switch (columnIndex)
		{
			case 0:
				return item.getPersonName();
			case 1:
				return item.getPersonId();
			case 2:
				return item.getRoiName();
			case 3:
				return item.getDate();
			case 4:
				return item.getTime();
			case 5:
				return item.getType();
			default:
				return null;
		}
	}
	
}
